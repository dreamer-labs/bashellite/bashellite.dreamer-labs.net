import re

from docutils.parsers.rst import directives
from sphinx.directives.other import TocTree


def string_list(argument):
    """Converts a space separated list of values into a Python list of strings.
    :param argument: The argument value
    :returns: A list of strings
    :raises: ValueError if the list is malformed
    """
    args = argument.split(' ')

    if not len(args) >= 1:
        raise ValueError('Arg list must contain values space or comma separated')

    return args


def duplicate_depth(argument):
    return directives.choice(argument, ('children', 'parent'))


class TocTreeFilter(TocTree):
    """
    Directive to notify Sphinx about the hierarchical structure of the docs,
    and to include a table-of-contents like tree in the current document. This
    version filters the entries based on a list of prefixes.
    """

    _option_spec = {'exclude-patterns': string_list,
                    }

    def filter_entries(self, node):
        toctree = node[0].children[0]
        patterns = []
        for pattern in self.options['exclude-patterns']:
            patterns.append(re.compile(pattern))

        entries = []
        for entry in toctree['entries']:
            matched = False
            for pattern in patterns:
                if pattern.match(entry[1]):
                    matched = True
                    break

            if not matched:
                entries.append(entry)
        toctree['entries'] = entries
        return node

    def run(self):
        ret = super().run()
        if 'exclude-patterns' in self.options:
            return self.filter_entries(ret)
        else:
            return ret


def setup(app):
    TocTreeFilter.option_spec.update(TocTreeFilter._option_spec)
    app.add_directive('toctree-filter', TocTreeFilter)
    return {'version': '0.1.0'}
