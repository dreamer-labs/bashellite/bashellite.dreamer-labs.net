import re

from pyspelling.filters.context import ContextFilter


class SimplerContextFilter(ContextFilter):

    def setup(self):
        """Setup."""

        self.context_visible_first = self.config['context_visible_first']
        self.delimiters = []
        self.escapes = None
        self.line_endings = self.config['normalize_line_endings']

        for delimiter in self.config['delimiters']:
            if not isinstance(delimiter, dict):
                continue
            self.delimiters.append({
                'open': re.compile(delimiter['open'], re.MULTILINE),
                'close': re.compile(delimiter['close'], re.MULTILINE)
            })

    def _filter(self, text):
        """Context delimiter filter."""

        if self.context_visible_first:
            return self._remove_text(text)
        else:
            return self._select_text(text)

    def _select_text(self, text):
        pass

    def _remove_text(self, text):
        """Removes matched text from start regex to end regex"""
        intervals = []

        for delim in self.delimiters:

            start = 0

            while start <= len(text):
                mopen = delim['open'].search(text, start)

                if mopen:
                    mclose = delim['close'].search(text, mopen.span()[1])
                    if mclose:
                        intervals.append((mopen.span()[0], mclose.span()[1]))
                        # Move start to then end of this match
                        start = mclose.span()[1]
                    else:
                        # Skip to next potential start block match as no close was found
                        start = mopen.span()[1]
                else:
                    break

        return chopped_text(text, intervals)


def chopped_text(text, intervals):
    end_include = len(text)
    start_include = 0

    new_text = ''
    # Moving backwards through the "text" add text outside chopped areas
    for chop_index in reversed(merge_intervals(intervals)):
        start_include = chop_index[1]
        new_text = text[start_include:end_include] + new_text

        start_include = 0
        end_include = chop_index[0]

    return text[:end_include] + new_text


def merge_intervals(intervals):
    """A simple algorithm can be used:
    1. Sort the intervals in increasing order
    2. Push the first interval on the stack
    3. Iterate through intervals and for each one compare current interval
    with the top of the stack and:
        A. If current interval does not overlap, push on to stack
        B. If current interval does overlap, merge both intervals in to one
        and push on to stack
    4. At the end return stack
    """
    si = sorted(intervals, key=lambda tup: tup[0])
    merged = []

    for tup in si:
        if not merged:
            merged.append(tup)
        else:
            b = merged.pop()
            if b[1] >= tup[0]:
                new_tup = tuple([b[0], tup[1]])
                merged.append(new_tup)
            else:
                merged.append(b)
                merged.append(tup)
    return merged


def get_plugin():
    """Return the filter."""

    return SimplerContextFilter
