import argparse
import datetime
import glob
import shutil
import os

import jinja2
import pypandoc
import requests
import yaml

import urllib.parse as url_parse

from git import Repo


def build_args():
    """CLI args object unparsed"""
    args = argparse.ArgumentParser('piperci release fetcher')

    release_group = args.add_mutually_exclusive_group(required=True)

    release_group.add_argument(
        '--release-file-uri',
        help='The location of the release config file '
             'to load componants from.')

    release_group.add_argument('--docs-yaml-uri',
                               help='Use a specific docs.yml at uri or file path')

    args.add_argument('--release',
                      default='altstadt',
                      help='The current release to build against: %(defaults)s')

    args.add_argument('--build-dir',
                      default='build',
                      help='The build staging directory or dest to copy README files'
                           'default: %(defaults)s')

    args.add_argument('--cache-dir',
                      default=os.path.join('cache', 'compenents'),
                      help='The cache directory to save compenents in'
                           'default: %(defaults)s')

    args.add_argument('--use-cached',
                      action='store_true',
                      help='Don\'t download components if they exist in cache-dir')

    args.add_argument('--component-requirements',
                      action='store_true',
                      help='Generate a requirements file for components in build-dir')

    return args


def build_target(dest_dir):

    if os.path.isdir(dest_dir):
        return False
    else:
        os.makedirs(dest_dir)
        return True


def convert_md(file_info, src_dir, dst_dir, template_env):
    md_files = []

    for file_path in glob.iglob(os.path.join(src_dir, file_info['path'])):
        name = os.path.splitext(file_info['name'])[0]
        file_path_dst = os.path.join(dst_dir, f'{name}.rst')

        pypandoc.convert_file(file_path,
                              'rst',
                              format='gfm',
                              outputfile=file_path_dst)
        if file_info.get('add_toc'):
            file_info['toc'].remove(file_info['name'])

            with open(file_path_dst, 'r') as original:
                data = original.read()

            with open(file_path_dst, 'w') as outfile:
                template = template_env.get_template('auto_toc.j2')
                outfile.write(data
                              + template.render(component=file_info['component'],
                                                toc=file_info['toc']))
        md_files.append(file_path_dst)

    return md_files


def copy_local_dir(src, dst, use_cached=False):
    src = resolve_path(src)
    dst = resolve_path(dst)

    if os.path.exists(dst):
        shutil.rmtree(dst)

    if use_cached:
        if not os.path.exists(dst):
            raise FileNotFoundError(f'{dst} does not exist')

        return {'version_info': 'local',
                'source_dir': dst}
    else:
        if os.path.isdir(src):
            shutil.copytree(src, dst, copy_function=shutil.copy)
        else:
            raise FileNotFoundError(f'{src} does not exist')

    return {'version_info': 'local',
            'source_dir': dst}


def copy_raw_file(file_info, src_dir, dst_dir):
    rst_files = []
    for file_path in glob.iglob(os.path.join(src_dir, file_info['path'])):
        file_path_dst = os.path.join(dst_dir, os.path.basename(file_path))
        if not os.path.exists(file_path):
            raise FileNotFoundError(f'{file_path} does not exist')
        shutil.copyfile(file_path, file_path_dst)
        rst_files.append(os.path.join(dst_dir, file_path_dst))
    return rst_files


def copy_rst(file_info, src_dir, dst_dir, template_env):

    rst_files = []

    for file_path in glob.iglob(os.path.join(src_dir, file_info['path'])):
        file_path_dst = os.path.join(dst_dir, os.path.basename(file_path))
        shutil.copyfile(file_path, file_path_dst)
        if file_info.get('add_toc'):
            file_info['toc'].remove(file_info['name'])
            with open(file_path_dst, 'a') as outfile:
                template = template_env.get_template('auto_toc.j2')
                outfile.write(template.render(component=file_info['component'],
                                              toc=file_info['toc']))
        rst_files.append(os.path.join(dst_dir, file_path_dst))

    return rst_files


def dictish(object, keys):
    """Returns a dictionary ensuring keys exist in object"""
    if not isinstance(object, dict):
        things = listish(object)
        object = {}
        for idx, key in enumerate(keys):
            try:
                object[key] = things[idx]
            except IndexError:
                pass

        if idx >= len(things):
            object['extras'] = things[idx:]

    for key in keys:
        if key not in object:
            object[key] = None
    return object


def fetch_components(components, dst_path, use_cached=False):
    """Clone target source repositories to the src_path.
    :components list: A list of dictionaries with the components section of a release
    :dst_path str: A string path where the repositories should be cloned
    :return: The list of components
    """

    for component in components:
        repo_dir = os.path.join(dst_path, component['name'])

        if dst_path not in repo_dir:
            raise RuntimeError(f'{repo_dir} is not under {dst_path}')

        comp_uri = url_parse.urlparse(component['uri'])
        if os.path.splitext(comp_uri.path)[1] == '.git':
            repo, comp_meta = git_clone(component['uri'],
                                        repo_dir,
                                        component['version'],
                                        use_cached=use_cached)
            component.update(comp_meta)
        else:
            comp_meta = copy_local_dir(component['uri'], repo_dir, use_cached)
            component.update(comp_meta)

    return components


def git_clone(component_uri, dest_dir, version='master', use_cached=False):
    comp_meta = {}
    if use_cached and os.path.exists(dest_dir):
        repo = Repo(dest_dir)
        comp_meta['version_info'] = {'commit': repo.head.commit.hexsha}

        comp_meta['source_dir'] = dest_dir
    else:
        if os.path.isdir(dest_dir):
            shutil.rmtree(dest_dir)
        repo = Repo.clone_from(component_uri,
                               dest_dir,
                               branch=version)

        comp_meta['version_info'] = {'commit': repo.head.commit.hexsha}
        comp_meta['source_dir'] = repo.working_dir if repo else None

    return repo, comp_meta


def gen_auto_readme(file_info, src_dir, dst_dir, template_env):
    outfiles = []
    template = template_env.get_template('auto_readme.rst.j2')

    rst_file_name = f'README{os.path.extsep}rst'
    file_path_dst = os.path.join(dst_dir, rst_file_name)
    with open(file_path_dst, 'w') as outfile:
        outfile.write(template.render(name=file_info['name'],
                                      title=file_info.get('title', file_info['name']),
                                      description=file_info.get('description'),
                                      file_type=file_info['type'],
                                      component=file_info['component'],
                                      toc=file_info['toc'],
                                      date=datetime.datetime.now(),
                                      version_info=file_info['version_info']))
    outfiles.append(file_path_dst)
    return outfiles


def gen_autodoc(file_info, src_dir, dst_dir, template_env):
    outfiles = []
    template = template_env.get_template('autodoc_template.rst.j2')

    rst_file_name = f'{file_info["name"]}{os.path.extsep}rst'
    file_path_dst = os.path.join(dst_dir, rst_file_name)
    with open(file_path_dst, 'w') as outfile:
        outfile.write(template.render(name=file_info['name'],
                                      title=file_info.get('title',
                                                          file_info['name']),
                                      description=file_info.get('description'),
                                      programs=file_info.get('programs', []),
                                      modules=file_info.get('modules', []),
                                      imports=file_info.get('imports', []),
                                      file_type=file_info['type'],
                                      date=datetime.datetime.now(),
                                      version_info=file_info['version_info']))
    outfiles.append(file_path_dst)
    return outfiles


def gen_component(src_dir, dst_dir, files=None, uri=None,
                  version_info=None,
                  template_dir=os.path.dirname(__file__)
                  + '/../source/_templates'):
    """Copy README files to the doc source tree for each componant.
    :src_dir str: Path to search for readme files
    :dst_dir str: Path to copy readme files to
    :files dict: The dictionary of the component's doc section
    :template_dir str: A path to the template_dir
    :uri str: The URI of the source component, used for external link back to project
    :return: (list, list)
    """

    template_dir = resolve_path(template_dir)
    loader = jinja2.FileSystemLoader(searchpath=template_dir)
    template_env = jinja2.Environment(loader=loader)
    template_env.lstrip_blocks = True
    template_env.trim_blocks = True
    doc_files = []
    requirements = []

    if not files:
        files = ['README.md', 'README.rst']

    files = normalize_files(files)
    toc = sorted([info['name'] for info in files])

    if uri:
        toc.append(f'Project Source <{uri}>')

    for file_info in files:
        if 'requirements' in file_info:
            requirements.extend(listish(file_info['requirements']))

        file_info['toc'] = toc
        file_info['version_info'] = version_info
        file_info['component'] = os.path.basename(src_dir)

        if file_info['type'] == 'rst':
            doc_files.extend(copy_rst(file_info, src_dir, dst_dir,
                                      template_env))

        elif file_info['type'] == 'md':
            doc_files.extend(convert_md(file_info, src_dir, dst_dir,
                                        template_env))

        elif file_info['type'] == 'text':
            doc_files.extend(gen_raw_file(file_info, src_dir, dst_dir,
                                          template_env, 'raw_file_template.rst.j2',
                                          {
                                              'name': file_info['name'],
                                              'highlight': file_info['highlight'],
                                              'description': file_info['description'],
                                              'add_toc': file_info['add_toc']
                                          }))

        elif file_info['type'] == 'openapi':
            doc_files.extend(gen_raw_file(file_info, src_dir, dst_dir,
                                          template_env, 'openapi_template.rst.j2',
                                          {
                                              'name': file_info['name'],
                                              'description': file_info['description'],
                                              'add_toc': file_info['add_toc'],
                                          }))

        elif file_info['type'] == 'autodoc':
            doc_files.extend(gen_autodoc(file_info, src_dir, dst_dir, template_env))

        elif file_info['type'] == 'index':
            doc_files.extend(gen_auto_readme(file_info, src_dir, dst_dir, template_env))

        else:
            doc_files.extend(copy_raw_file(file_info, src_dir, dst_dir))

    return doc_files, requirements


def gen_raw_file(file_info, src_dir, dst_dir,
                 template_env, template_name, template_data):
    text_files = []

    for file_path in glob.iglob(os.path.join(src_dir, file_info['path'])):
        template = template_env.get_template(template_name)

        rst_file_name = (os.path.splitext(file_info['name'])[0]
                         + os.path.extsep
                         + 'rst')
        raw_file_name = os.path.basename(file_path)
        file_path_dst = os.path.join(dst_dir, rst_file_name)
        raw_file_path_dst = os.path.join(dst_dir, f'{raw_file_name}')
        template_data['path'] = raw_file_name

        with open(file_path_dst, 'w') as outfile:
            outfile.write(template.render(date=datetime.datetime.now(),
                                          version_info=file_info['version_info'],
                                          **template_data))

        shutil.copyfile(file_path, raw_file_path_dst)

    try:
        text_files.append(file_path_dst)
        text_files.append(raw_file_path_dst)
    except Exception:
        pass

    return text_files


def get_docs_yml(docs_uri):
    if isinstance(docs_uri, str):
        docs_uri = url_parse.urlparse(docs_uri)

    if docs_uri.scheme and docs_uri.netloc:
        resp = requests.get(docs_uri.geturl())
        resp.raise_for_status()
        text = resp.text
    else:
        with open(resolve_path(docs_uri.geturl()), 'r') as docs_file:
            text = docs_file.read()

    return yaml.safe_load(text)


def get_docs_from_release(uri, release):
    """Fetch the release file from uri.
    :uri str: A file path or web uri to a piperci release meta yaml file
    :return: Dictionary loaded releases file
    """

    base_def_uri = url_parse.urlparse(uri)

    resp = requests.get(base_def_uri.geturl())
    resp.raise_for_status()

    release_base = yaml.safe_load(resp.text)

    doc_path = release_base['releases'][release]['components']['docs']['config']

    docs_uri = base_def_uri._replace(path=os.path.join(
        os.path.dirname(base_def_uri.path), doc_path))

    return get_docs_yml(docs_uri)


def listish(object):
    """Returns a list of object or object if it is already a list"""

    if isinstance(object, list):
        return object
    else:
        return [object]


def normalize_autodoc(file):

    if 'modules' in file:

        file['modules'] = listish(file['modules'])
        modules = []
        for module in file['modules']:
            module = dictish(module, ('module', 'description',
                                      'usage', 'imports'))
            if module['imports']:
                module['imports'] = listish(module['imports'])
            modules.append(module)

        file['modules'] = modules

    if 'imports' in file:
        file['imports'] = listish(file['imports'])

    if 'programs' in file:
        file['programs'] = listish(file['programs'])

    return file


def normalize_files(files):
    """Normalize the list of files into the appropriate list of dictionaries.
    :files list: list of files to copy.
    :return: list of dictionaries
    """
    new_files = []
    for file in files:
        if isinstance(file, str):
            file = {'name': file if '/' not in file else os.path.basename(file),
                    'path': file}

        if 'name' not in file:
            raise ValueError(f'Dictionary does not contain the required key "name"')

        if 'path' not in file:
            file['path'] = file['name']

        if os.path.extsep in file['name']:
            file['name'] = os.path.splitext(file['name'])[0]

        if 'type' not in file:
            basename, ext = os.path.splitext(file['path'])
            if ext in ('.md', '.rst', '.py', '.png'):
                file['type'] = ext.strip('.')
            else:
                file['type'] = 'text'

        if file['type'] == 'autodoc':
            file = normalize_autodoc(file)

        if 'highlight' not in file:
            file['highlight'] = 'text'

        if file['type'] == 'text':
            file = dictish(file, ('description', 'add_toc', 'highlight'))

        if file['type'] == 'openapi':
            file = dictish(file, ('description', 'add_toc'))

        new_files.append(file)

    return new_files


def main():

    args = build_args()
    opts = args.parse_args()

    repo_base_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../'))
    src_path = os.path.join(repo_base_path, opts.cache_dir)
    dst_path = os.path.join(repo_base_path, opts.build_dir)

    if opts.docs_yaml_uri:
        docs_def = get_docs_yml(opts.docs_yaml_uri)
    elif opts.release_file_uri:
        docs_def = get_docs_from_release(opts.release_file_uri, opts.release)
    else:
        print('Define either a release file uri or docs.yml uri')
        raise SystemExit(1)

    components = fetch_components(docs_def['docs'], src_path,
                                  use_cached=opts.use_cached)

    clean_list, doc_files, requirements = build_components(components,
                                                           src_path,
                                                           dst_path)

    if opts.component_requirements:
        with open(os.path.join(dst_path, 'component-requirements.txt'),
                  'w') as comp_file:
            comp_file.writelines(req + '\n' for req in set(requirements))

    nl = '\n'
    print('Created:\n', f'{nl.join(clean_list)}')
    print('Copied:\n', f'{nl.join(doc_files)}')

    with open('.cleanup', 'a') as cleanups:

        cleanups.writelines(line + '\n' for line in clean_list)
        cleanups.writelines(line + '\n' for line in doc_files)


def build_components(components, src_path, dst_path):

    clean_list = []
    doc_files = []
    requirements = []
    for component in components:

        if component['source_dir']:
            files = component['files'] if 'files' in component else []

            if component.get('site_root'):
                # site_root component location at dst_path
                component['dest_dir'] = dst_path
            else:
                component['dest_dir'] = os.path.join(dst_path, component['name'])

            if build_target(component['dest_dir']):
                clean_list.append(component['dest_dir'])

            component['docs'], comp_reqs = gen_component(
                component['source_dir'],
                component['dest_dir'],
                files,
                uri=component['uri'],
                version_info=component['version_info'])
            doc_files.extend(component['docs'])

        if 'requirements' in component:
            requirements.extend(listish(component['requirements']))

        if len(comp_reqs):
            requirements.extend(comp_reqs)

    return clean_list, doc_files, requirements


def resolve_path(path):
    return os.path.realpath(os.path.expandvars(os.path.expanduser(path)))


if __name__ == '__main__':
    main()
