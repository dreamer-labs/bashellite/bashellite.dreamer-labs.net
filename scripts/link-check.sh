#!/bin/bash


if [[ -z "${DOCKER_MOUNT}" ]];
then
  echo '[ERROR] Set DOCKER_MOUNT to where the source code is mounted'
else
  docker run --entrypoint "" -v $DOCKER_MOUNT:/source linkchecker/linkchecker /bin/bash -c 'cd /source/artifact; python -m SimpleHTTPServer &  linkchecker --check-extern --ignore-url _static http://localhost:8000/index.html; kill %1'
fi
